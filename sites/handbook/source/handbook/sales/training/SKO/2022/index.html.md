---
layout: handbook-page-toc
title: "Sales Kickoff 2022"
description: "GitLab Sales Kickoff 2022"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
GitLab Sales Kickoff 2022 will be held virtually during the week of March 14-18, 2022.

## SKO 2022 Event Promo Video

Coming Soon 

# SKO 2022 Day 1 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Welcome to GitLab Sales Kickoff 2022 | slides | video |
| Product Keynote | slides | video |
| Field Spotlight #1 | no slides | video |
| Customer Speaker | slides | video |
| Field Spotlight #2 | no slides | video |

# SKO 2022 Day 2 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Day 2 Opening | no slides | video |
| Motivational Speaker | no slides | video |
| Channel & Alliances Keynote | slides | video |
| Field Spotlight #3 | no slides | video |
| Marketing Keynote | slides | video |
| Field Spotlight #4 | no slides | video |
| Closing Keynote | slides| video |

# SKO Awards Ceremony

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Awards Ceremony | slides | video |
| President's Club | slides | video |

# SKO 2022 Role-Based Breakout Sessions

## Enterprise Sales Strategic Account Leaders

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Building a Strategic Partnership with your Customer with Value Stream Assessments | slides | video |
| Supersonic pipeline: Best practices for prospecting to 4x | slides | video |
| Using Effective Proposals to Advance and Win Deals | slides | video |

## Commercial Sales Account Executives

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| The Big Pitch | slides | video |
| Finding the "Why Now" | slides | video |
| The Journey to Ultimate, Part 2 | slides | video |

## Channel Sales Account Managers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Developing your Select Partners to their Maximum Potential and Value Opportunity | slides | video |
| Building Pipeline with your Partners MDF and Marketing Campaigns | slides | video |
| TBD | slides | video |

## Solution Architects

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Winning in CI/CD Against GitHub | slides | video |
| An Introduction to Value Stream Assessments | slides | video |
| Positioning the GitLab Agent for Kubernetes in your sales deal | slides | video |

## Technical Account Managers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| TAM Strategy | slides | video |
| TAM and Customer Personas | slides | video |
| CI/CD Workshop | slides | video |

## Professional Services Engineers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| TBD | slides | video |
| TBD | slides | video |
| TBD | slides | video |

## Sales Development Reps

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Partnering with Sales: Good to Great | slides | video |
| Alumni Stories: Lessons Learned | slides | video |
| TBD | slides | video |

## Field Marketing

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Field Marketing Goals for FY23 | slides | video |

